// Task №1
// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості 
//значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
let name = 'Nikita';
let admin = name; 

console.log(admin);

// Task №2
// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
let days = 9;
let hour = 24;
let minute = 60;
let second = 60;
let x = (second * minute * hour * days);

console.log(x); 

// Task №3 
// Запитайте у користувача якесь значення і виведіть його в консоль. 
let num = prompt('What is your favorite number?');

console.log(num);  